<?php

include_once("IModel.php");

class DBModel implements IModel {
	protected $db = null;

	public function __construct($db = null){
	
		$servername = "localhost";
		$username = "root";
		$password = "";
		
		$this->xml = new DOMDocument;
		$this->xml->load('SkierLogs.xml');
		
		if($db){
		  $this->db = $db;
		}
		else{
			try {
				$this->db = new PDO("mysql:host=$servername;dbname=skierlogs", $username, $password);
				$this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				}
			catch(PDOException $e)
				{
				echo "Connection failed: " . $e->getMessage();
				}
		}
	}
 
	public function getNode($el, $tag){
		return $el->getElementsByTagName($tag)->item(0)->textContent;
  }

	public function importSkiers(){
		$xpath = new DOMXpath($this->xml);
		$skiers = $xpath->query("//SkierLogs/Skiers/Skier");
		
		if (!is_null($skiers)){
		  foreach ($skiers as $skier) {
			$stmt = $this->db->prepare("INSERT INTO skiers(userName, firstName, lastName, yearOfBirth) VALUES(?,?,?,?)");
				$stmt->bindValue(1, $skier->getAttribute('userName'), PDO::PARAM_STR);
				$stmt->bindValue(2, $this->getNode($skier, 'FirstName'), PDO::PARAM_STR);
				$stmt->bindValue(3, $this->getNode($skier, 'LastName'), PDO::PARAM_STR);
				$stmt->bindValue(4, $this->getNode($skier, 'YearOfBirth'), PDO::PARAM_INT);
			$stmt->execute();
		  }
		}
  }

	public function importClubs(){
		$xpath = new DOMXpath($this->xml);
		$clubs = $xpath->query("//SkierLogs/Clubs/Club");
		
		if (!is_null($clubs)){
		  foreach ($clubs as $club) {
			$stmt = $this->db->prepare("INSERT INTO clubs(clubID, clubName, city, county) VALUES (?,?,?,?)");
				$stmt->bindValue(1, $club->getAttribute('id'), PDO::PARAM_STR);
				$stmt->bindValue(2, $this->getNode($club, 'Name'), PDO::PARAM_STR);
				$stmt->bindValue(3, $this->getNode($club, 'City'), PDO::PARAM_STR);
				$stmt->bindValue(4, $this->getNode($club, 'County'), PDO::PARAM_STR);
			$stmt->execute();
		   }
		}
  }

    public function importSeason(){
		$xpath = new DOMXpath($this->xml);
		$season = $xpath->query("//SkierLogs/Season");
		
		if(!is_null($season)){
		  foreach ($season as $ski) {
			$stmt = $this->db->prepare("INSERT INTO season(fallYear, clubId, userName, totalDistance) VALUES(?,?,?,?)");
			$stmt->bindValue(1, $ski->getAttribute('fallYear'), PDO::PARAM_INT);
			
			//echo $ski->getAttribute('fallYear')."<br> ";
			
			$skiers = $xpath->query("Skiers", $ski);
			
			foreach ($skiers as $club){
				$clubName = $club->getAttribute('clubId');
				if ($clubName == '') {
					$clubName = NULL;
				}
				$stmt->bindValue(2, $clubName, PDO::PARAM_STR);
				
			//	echo '-'. $club->getAttribute('clubId')."-<br> ";
				
				$skier = $xpath->query("Skier", $club);
				foreach ($skier as $skier){
					$stmt->bindValue(3, $skier->getAttribute('userName'), PDO::PARAM_STR);
					
				//	echo $skier->getAttribute('userName')." ";
					
					$log = $xpath->query("Log/Entry", $skier);
					$totalDistance = 0;
					//var_dump($log);
					foreach ($log as $entry){
								
						$totalDistance += $this->getNode($entry, 'Distance');
					
					}
					$stmt->bindValue(4, $totalDistance, PDO::PARAM_INT);
					
					$stmt->execute();
					
					}
					//echo "<br>";
				}
			}
			 
			
		  }
	}

	

}

?>